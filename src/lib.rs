use std::collections::HashMap;
use regex::Regex;
use tui::layout::{Alignment, Constraint, Direction, Layout, Rect};
use tui::style::{Color, Style};
use tui::widgets::{Block, BorderType, Borders, Paragraph};
use tui::{backend::Backend, Frame};

enum GameType {
    Multiplayer,
    Singleplayer,
}

pub struct Game {
    mode: GameType,
    state: GameState,
    wordlist: Vec<Vec<char>>,
    pub game_over: bool,
    pub won: bool,
    pub word: Vec<char>,
}

struct GameState {
    guessed_letters: HashMap<char, bool>,
    game_board: [[(char, Color); 5]; 6],
    row: usize,
    col: usize,
}

impl Game {
    pub fn new(wordlist: Vec<Vec<char>>, word: Vec<char>) -> Game {
        Game {
            word: word,
            wordlist: wordlist,
            mode: GameType::Singleplayer,
            state: GameState {
                guessed_letters: HashMap::new(),
                game_board: [[(' ', Color::Gray); 5]; 6],
                row: 0,
                col: 0,
            },
            game_over: false,
            won: false
        }
    }

    pub fn add_char(&mut self, c: char) {
        //TODO: elimnate non alphas
        let re = Regex::new("^[a-zA-Z]+$").unwrap();

        if self.state.col < 5 && re.is_match(c.to_string().as_str()) {
            self.state.game_board[self.state.row][self.state.col] =
                (c.to_ascii_uppercase(), Color::Gray);
            self.state.col += 1;
        }
    }

    pub fn del_char(&mut self) {
        #![allow(unused_comparisons)]
        if self.state.col > 0 {
            self.state.col -= 1;
            self.state.game_board[self.state.row][self.state.col] = (' ', Color::Gray);
        }
    }
    
    pub fn guess(&mut self) {
        if self.state.col < 5 { return; }
        
        let guessed_word = self.state.game_board[self.state.row].iter().map(|s| s.0).collect::<Vec<_>>();
        if !(self.wordlist.contains(&guessed_word)) { return; }
        if guessed_word == self.word {
            self.game_over = true;
            self.won = true;

        } else {
            for (i, c) in guessed_word.into_iter().enumerate() {
                if self.word[i] == c {
                    self.state.game_board[self.state.row][i] = (c, Color::Green);
                    
                } else if self.word.contains(&c) {
                    self.state.game_board[self.state.row][i] = (c, Color::Yellow);
                    
                } else {
                    self.state.game_board[self.state.row][i] = (c, Color::Black);
                }

                self.state.guessed_letters.insert(c, true);
            }

            self.state.row += 1;
            self.state.col = 0;
            if self.state.row == 6 { self.game_over = true; }
        }
    }
}

// master draw function
pub fn draw<B: Backend>(f: &mut Frame<B>, game: &mut Game) {
    let constraints = match game.mode {
        GameType::Singleplayer => [Constraint::Percentage(100)].as_ref(),
        GameType::Multiplayer => [Constraint::Percentage(50), Constraint::Percentage(50)].as_ref(),
    };

    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(f.size());

    for chunk in chunks {
        let block = Block::default().borders(Borders::ALL);
        f.render_widget(block, chunk);

        draw_game(f, game, chunk);
    }
}

fn draw_game<B: Backend>(f: &mut Frame<B>, game: &mut Game, area: Rect) {
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Percentage(10),
                Constraint::Percentage(80),
                Constraint::Percentage(10),
            ]
            .as_ref(),
        )
        .direction(Direction::Vertical)
        .split(area);

    draw_board(f, game, chunks[1]);
}

fn draw_stats<B: Backend>(f: &mut Frame<B>, game: &mut Game, area: Rect) {}

fn draw_board<B: Backend>(f: &mut Frame<B>, game: &mut Game, area: Rect) {
    let h_pad = (area.width - 50) / 2;
    let v_pad = (area.height - 24) / 2;

    let h_rects = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(vec![
            Constraint::Min(h_pad),
            Constraint::Length(50),
            Constraint::Min(h_pad),
        ])
        .split(area);

    let board_block = Block::default()
        .borders(Borders::ALL)
        .border_type(BorderType::Thick);
    f.render_widget(board_block, h_rects[1]);

    let row_constraints = vec![
        Constraint::Min(v_pad),
        Constraint::Length(4),
        Constraint::Length(4),
        Constraint::Length(4),
        Constraint::Length(4),
        Constraint::Length(4),
        Constraint::Length(4),
        Constraint::Min(v_pad),
    ];

    let h_pad = (h_rects[1].width - 40) / 2;
    let col_constraints = vec![
        Constraint::Min(h_pad),
        Constraint::Length(8),
        Constraint::Length(8),
        Constraint::Length(8),
        Constraint::Length(8),
        Constraint::Length(8),
        Constraint::Min(h_pad),
    ];

    let game_board = game.state.game_board;
    let row_rects = Layout::default()
        .constraints(row_constraints)
        .direction(Direction::Vertical)
        .split(h_rects[1]);

    for r in 0..6 {
        let col_rects = Layout::default()
            .constraints(col_constraints.clone())
            .direction(Direction::Horizontal)
            .split(row_rects[r + 1]);

        for c in 0..5 {
            let text = Paragraph::new(game_board[r][c].0.to_string())
                .alignment(Alignment::Center)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .border_type(BorderType::Rounded)
                        .border_style(Style::default().fg(game_board[r][c].1)),
                );

            f.render_widget(text, col_rects[c + 1]);
        }
    }
}

fn draw_alphabet<B: Backend>(f: &mut Frame<B>, game: &mut Game, area: Rect) {}
