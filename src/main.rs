use crossterm::{
    event::{self, Event, KeyCode},
    QueueableCommand,
};
use rand::Rng;
use std::{
    fs,
    io::{self, Write},
    thread, time,
};
use tui::{backend::CrosstermBackend, Terminal};

use wordle::{draw, Game};

fn main() -> Result<(), io::Error> {
    crossterm::terminal::enable_raw_mode()?;

    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.clear()?;
    terminal.hide_cursor()?;

    let wordlist: Vec<Vec<char>> = fs::read_to_string("wordlist.txt")
        .unwrap()
        .split("\n")
        .map(|s| s.chars().map(|c| c.to_ascii_uppercase()).collect())
        .collect::<Vec<_>>();
    let answers: Vec<Vec<char>> = fs::read_to_string("wordlist_answers.txt")
        .unwrap()
        .split("\n")
        .map(|s| s.chars().map(|c| c.to_ascii_uppercase()).collect())
        .collect::<Vec<_>>();

    let word = answers[rand::thread_rng().gen_range(0..answers.len())].clone();
    assert!(wordlist.contains(&word));

    let mut game = Game::new(wordlist, word.clone());
    let mut last_tick = time::Instant::now();
    let tick_rate = time::Duration::from_millis(100);

    loop {
        terminal.draw(|f| draw(f, &mut game))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| time::Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Esc => {
                        game.game_over = true;
                    }
                    KeyCode::Char(c) => {
                        game.add_char(c);
                    }
                    KeyCode::Backspace => {
                        game.del_char();
                    }
                    KeyCode::Enter => {
                        game.guess();
                    }
                    _ => {}
                }
            }
        }
        if last_tick.elapsed() >= tick_rate {
            last_tick = time::Instant::now();
        }
        if game.game_over {
            break;
        }
    }

    terminal.clear()?;
    terminal.show_cursor()?;
    crossterm::terminal::disable_raw_mode()?;

    io::stdout().queue(crossterm::cursor::RestorePosition)?;
    io::stdout().flush()?;
    thread::sleep(time::Duration::from_secs(1));

    if game.won {
        println!("You Won!");
    } else {
        println!("Game Over");
    }

    let word: String = word.into_iter().collect();
    println!("Word was: {}", word);

    Ok(())
}
